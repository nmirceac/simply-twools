<?php

namespace App;

class Utils {
    public static function getAbsoluteUrl($route)
    {
        return rtrim(\Config::get('web_ext.host').preg_replace('#/+#', '/',\Config::get('web_ext.basePath'),'/').'/'.$route);
    }
    
    public static function getAbsolutePath($route)
    {
        return preg_replace('#/+#', '/',rtrim(\Config::get('web_ext.basePath'),'/').'/'.$route);
    }
    
    public static function getBasePath()
    {
        return \Config::get('web_ext.basePath');
    }
    
    public static function currentDate($format="Y-m-d")
    {
        return date($format);
    }
    
    public static function date($date,$format="Y-m-d")
    {
        return date($format,strtotime($date));;
    }
    
    public static function getThumbPath($imgPath=0,$size='600x300',$pathPrefix='',$suffix='')
    {
        if($imgPath===0)
            return false;
        return '/'.ltrim(\Config::get('web_ext.basePath').\Simply\Web::route('utils-thumbs',base64_encode(json_encode(array('path'=>$pathPrefix.$imgPath.$suffix,'size'=>$size)))),'/');
    }
    
    public static function count($array=0)
    {
        if(is_array($array))
            return count($array);
        return false;
    }
}

class Files {
    public static function getFromPath($path,$extensions=array())
    {
        $files=array_slice(scandir($path),2);
        if(empty($extensions))
            return $files;
        
        if(!is_array($extensions))
            $extensions=explode(',',$extensions);
            
        foreach($extensions as $id=>$ext)
            $extensions[$id]=strtolower(trim($ext,' .'));
            
        $filter=array();
        foreach($files as $file)
        {
            $ext=strtolower(substr($file,strrpos($file,'.')+1));
            if(in_array($ext,$extensions))
                $filter[]=$file;
        }
        
        return $filter;
    }
}


class LessTools {
    public static function checkCss($src=0,$dst=0)
    {
        if($src===0 or $dst===0)
            return false;
        if(!file_exists($src))
            throw new \Exception('Cannot find or access the less source file '.$src);
        if(!file_exists($dst))
            $dstMTime=0;
        else
        {
            $dstMTime=filemtime($dst);
            if(filemtime($src)>$dstMTime)
                $dstMTime;
        }
        
        return array('mtime'=>$dstMTime,'compiled'=>$dst,'source'=>$src);
    }
    
    public static function buildCss($src=0,$dst=0)
    {
        if($src===0 or $dst===0)
            return false;
        if(!file_exists($src))
            throw new \Exception('Cannot find or access the less source file '.$src);
        
        require_once('../vendor/oyejorge/less.php/lessc.inc.php');
        $options = array( 'compress'=>true );
        $parser = new \Less_Parser($options);
        
        $parser->parseFile($src,\Config::get('web_ext.basePath').substr($src,0,strrpos($src,'/')+1));
        $compiled = $parser -> getCss();
        file_put_contents($dst,$compiled);
    }
    
    
}


class Identity {
    public static $details=array();
    
    public static function get($what)
    {
        if(isset(Identity::$details[$what]))
            return Identity::$details[$what];
        else
            return false;
    }
}

Identity::$details=array();
Identity::$details['name']="Motor Spares Stop";
Identity::$details['tel']="011 440 3335";
Identity::$details['fax']="011 440 3339";
Identity::$details['address']="628 Louis Botha Avenue, Wynberg";
Identity::$details['city']="Johannesburg";
Identity::$details['coordinates']="-26.122103,28.084987";

Identity::$details['strapline']="YOUR ONE STOP SPARES SHOP";
Identity::$details['schedule']="8:00am - 6:00pm Monday to Friday, 8:00am - 3:00pm Saturday, 8:00am - 1:00pm Sunday";
Identity::$details['email']="sales@motorsparestop.co.za";
Identity::$details['facebook']="480332298667889";
Identity::$details['googlePlus']="101059789040495686442";

Identity::$details['description']="We specialize in retail and wholesale of new generic motor spare parts. Our extremely competitive prices and our large quantities of stock give us the edge.";
Identity::$details['keywords']="automotive, spares, accessories, car body parts, automotive coating, retail, wholesale, bosch, castrol, fram, ferodo, shell, competitive prices";

Identity::$details['testimonials'][]=array('name'=>'Mircea','comment'=>'I really enjoyed working with the Motor spares team, they offer great client service!');
Identity::$details['testimonials'][]=array('name'=>'Grant','comment'=>'Best QUALITY AND BEST SERVICE, NO one can beat this price.');
#Identity::$details['testimonials'][]=array('name'=>'Jane Doe','comment'=>'cheapest and best!!');
#Identity::$details['testimonials'][]=array('name'=>'Jane Doe','comment'=>'cheapest and best!!');
#Identity::$details['testimonials'][]=array('name'=>'Jane Doe','comment'=>'cheapest and best!!');
#Identity::$details['testimonials'][]=array('name'=>'Jane Doe','comment'=>'cheapest and best!!');




